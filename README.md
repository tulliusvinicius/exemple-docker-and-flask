# Exemple Docker and Flask



## How to run:

### Install Docker 

https://docs.docker.com/get-docker/

### Download image Python or any other

docker pull python:3.10-bullseye

### Create image

docker build -t flask-app:dev-0.0.1 .

### Run
docker run -d -P flask-app:dev-0.0.1

ps.:  

-d = run in background
-P = get a host port and mapped a port from EXPOSE

Running on localhost:port