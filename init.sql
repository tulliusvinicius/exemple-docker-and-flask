-- Grant all privileges to the root user from all hosts
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
-- Set a password for the root user
ALTER USER 'root'@'%' IDENTIFIED BY 'root';

-- Create the 'simple_app' database if it doesn't exist
CREATE DATABASE IF NOT EXISTS simple_app;

-- Use the 'simple_app' database
USE simple_app;

-- Create a 'users' table if it doesn't exist
CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL
);

-- -- Insert some sample data
-- INSERT INTO users (name, email) VALUES
--     ('John Doe', 'john@example.com'),
--     ('Jane Smith', 'jane@example.com');
